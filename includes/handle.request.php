<?php
	defined('NOSPLODYPLEASE354') or die('Nope');

if (isset($_POST['hash0'])) {

//Variables - Global
	$hasposted = 1;
	require_once('riak-php-client/riak.php');

//Variables - Paste
	$paste = htmlentities($_POST['paste']);
	$hash1 = htmlentities($_POST['hash1']);
	$thisbucket = htmlentities($_POST['public']);
	
//Variables - Requests
	$requestaddress = $_SERVER['REMOTE_ADDR'];
	$requesttime = date('U');
	$requestarray = explode(':', $paste);
	
	
	if ($requestarray[0] == '[data]') {
		$data = array(
			'requesttype' => '[data]',
			'requesttime' => $requesttime,
			'requestaddress' => $requestaddress,
			'requesthash' => $hash1,
			'requestbucket' => $thisbucket,
			'requestogname' => $_FILES["file"]["name"],
			'requestdescription' => str_replace("[data]:", "", $paste),
			'requestcontenttype' => $_FILES["file"]["type"],
			'requestcontent' => base64_encode(file_get_contents($_FILES["file"]["tmp_name"]))
		);
	} else {
		switch ($requestarray[0]) {
			case '[code]':
				$requesttype = $requestarray[0];
				break;
			case '[image]':
				$requesttype = $requestarray[0];
				break;
			case '[link]':
				$requesttype = $requestarray[0];
				break;
			case '[text]':
				$requesttype = $requestarray[0];
				break;
			default:
				$requesttype = '[default]';
		}
		
		$data = array(
			'requesttype' => $requesttype,
			'requesttime' => $requesttime,
			'requestaddress' => $requestaddress,
			'requesthash' => $hash1,
			'requestbucket' => $thisbucket,
			'requestpaste' => str_replace("$requesttype:", "", $paste)	
		);
	}

	$json = json_encode($data);
	
/*
	$data[0] = $requesttype;
	$data[1] = $requesttime;
	$data[2] = $requestaddress;
	$data[3] = $hash1;
	$data[4] = $thisbucket;
	$data[5] = str_replace("$requesttype:", "", $paste);
*/

/*
	echo '<pre>';
	echo $json;
	echo '</pre>';
*/

	$client = new RiakClient($riakserver, 8098);
	$bucket = $client->bucket($thisbucket);
	$data = $bucket->newBinary($hash1, $json, 'text/json');
	$data->store();

	$line = "$thisbucket|$hash1\n";

	$fh = fopen($scriptlist, 'a') or die("can't open file");
	fwrite($fh, $line);
	fclose($fh);

	$_SESSION['hashPosted'] = 'thatistrue';
}
?>
