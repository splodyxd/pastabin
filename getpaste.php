<?php
	define('NOSPLODYPLEASE354', 'TRUE');

	require_once('riak-php-client/riak.php');

	include ('./includes/config.php');

	$client = new RiakClient($riakserver, 8098);
	$bucket = $client->bucket('pastebinpublic');
	$hash = $_GET['hash'];
	$paste = $bucket->get($hash);
	
	$data = $paste->data;
	
	switch($data['requesttype']) {
		case '[default]':
			$content="<pre><code>$data[requestpaste]</code></pre>";
			include ('./templates/request.body.php');
			break;
		case '[code]':
			$content="<pre><code>$data[requestpaste]</code></pre>";
			include ('./templates/request.body.php');
			break;
		case '[text]':
			$content="<pre><code>$data[requestpaste]</code></pre>";
			include ('./templates/request.body.php');
			break;
		case '[link]':
			header("Location: $data[requestpaste]");			
			break;
		case '[image]':
			header("Location: $data[requestpaste]");			
			break;
		case '[data]':
			header("Content-type: $data[requestcontenttype]");
			header("Content-Disposition: attachment; filename=$data[requestogname]");
			echo base64_decode($data['requestcontent']);
			break;
	}
?>
