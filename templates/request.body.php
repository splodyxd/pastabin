<html>
<head>
        <link rel="stylesheet" href="highlight.js/styles/default.css">
        <script src="highlight.js/highlight.pack.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
	<title><?php echo $sitenamelong . " - $hash" ; ?></title>
</head>
	<body>
		<div id='code'>
			<?php echo $content; ?>
		</div>

	<div>Formatted by <a href='https://github.com/isagalaev/highlight.js'>Highlight.js</a></div>
	<?php include('./templates/extra.php'); ?>	
	</body>
</html>
