<?php
?><!-- ------------------------ -->
<!-- Server: <?php echo $servername; ?> -->
<!-- Production-cut: <?php echo $serverproductionvalue; ?> -->
<!-- ------------------------ -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $sitenamelong; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php if (isset($_SESSION['hashPosted'])) { echo "<meta http-equiv=\"refresh\" content=\"0; url=http://$servername/#$_POST[hash0]\">" ; } ?>

    <!-- Le styles -->
    <!-- TODO: make sure bootstrap.min.css points to BootTheme generated file -->
        <link href="/style/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <!-- TODO: make sure bootstrap-responsive.min.css points to BootTheme generated file -->    
        <link href="/style/bootstrap-responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/style/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons >
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png"-->

    <script src="includes/js.js"></script>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#"><?php echo $sitenamelong; ?></a>
        </div>
      </div>
    </div>

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <form name="pasteit" action="http://<?php echo $servername; ?>/" method="post" enctype="multipart/form-data">
                        <textarea id='textarea' rows="20" style='width:100%;' name="paste" onkeyup="functionData()"></textarea><br>
                        <input name='hash0' type='hidden' value='<?php echo generateRandomString(); ?>'>
                        <input name='hash1' type='hidden' value='<?php echo generateRandomString(); ?>'>
                        <select name="public">
                                <option value="pastebinprivate" selected>Private [ Default ]</option>
                                <option value="pastebinpublic">Public [ Share with everyone ]</option>
                        </select>
                        <div id='qwerty'></div>
                        <div style='width: 100%; text-align: right;'><input class="btn btn-primary btn-large" type='submit' value='Submit'></div>
		</form>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4" style='text-align:center;'>
          <h2>Previous pasta</h2>
                        <?php
                                if (isset($_SESSION['hashPosted'])) {
                                        switch ($thisbucket) {
                                                case 'pastebinprivate':
                                                        echo 'Your previous pasta was private.';
                                                        break;
                                                case 'pastebinpublic':
                                                        echo "<a href='http://$servername/$hash1'>$hash1</a>";
                                                        break;
                                        }

					unset ($_SESSION['hashPosted']);
                                } else {
                                        echo "Paste something first";
                                }
                        ?>
        </div>
        <div class="span4" style='text-align:center;'>
          <h2>Recent pastas</h2>
                        <?php /*$getcount = 7 ;*/ include ('./includes/getlist.php');?>
       </div>
        <div class="span4" style='text-align:center;'>
          <h2>Available pasta tags</h2>
          <div style='border-bottom: 1px solid #b4b4b4; padding: 7px;'>[code/text]: (default), [link]: (url shortener), [image]: (url shortener for image links)</div>
          <div style='border-bottom: 1px solid #b4b4b4; padding: 7px;'><b>[image]</b>:(http://)example.com/image.jpg</div>
          <div style='border-bottom: 1px solid #b4b4b4; padding: 7px;'><b>[code]</b>:main () { cout << 'Goodbye Universe'; }</div>
          <div style='padding: 7px;'><b>[link]</b>:http://domain.com/reallylongurlstring-slash-filenamehere.html</div>
        </div>
      </div>

      <hr>

      <footer>
        <p><?php echo $sitenameshort; ?></p>
      </footer>

    </div> <!-- /container -->

	<!-- Extra Footer stuff--->
		<?php include ('./templates/extra.php');?>
	<!-- Extra Footer stuff--->
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="/style/jquery.min.js"></script>
        <script src="/style/bootstrap.min.js"></script>	
  </body>
</html>
